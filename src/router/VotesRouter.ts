import { Router } from 'express';
import VoteController from '../controllers/VoteController';

export default class VotesRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public routes() :void {
      this.router.get('/',  VoteController.getHandlerFor(null).bind(VoteController));
      this.router.get('/alliance',  VoteController.getHandlerFor(1).bind(VoteController));
      this.router.get('/horde',  VoteController.getHandlerFor(2).bind(VoteController));
      this.router.post('/',  VoteController.submitVote);
    }
}
