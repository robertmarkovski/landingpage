import * as express from 'express';
import VotesRouter from './VotesRouter';
import IServer from '../interfaces/ServerInterface';
import config from "../config/config";
import path = require('path')

export default class Routes {
    static init(server: IServer) :void {
        const router: express.Router = express.Router()

        router.get("/config", (req, res) => {
            res.json({ endDate: config.voteEndDate } );
        });

        router.get('/datenschutz', (req, res) => {
            res.sendFile(path.join(__dirname, '..', '..', 'public', 'html', 'datenschutz.html'))
        })

        router.get('/nutzungsbestimmungen', (req, res) => {
            res.sendFile(path.join(__dirname, '..', '..', 'public', 'html', 'nutzungsbestimmungen.html'))
        })

        router.get('/impressum', (req, res) => {
            res.sendFile(path.join(__dirname, '..', '..', 'public', 'html', 'impressum.html'))
        })

        server.app.use('/', router);
        server.app.use('/api/votes/', new VotesRouter().router);
    }
}
