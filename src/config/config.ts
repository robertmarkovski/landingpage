
export default {
  recaptchaSecret: "6LcWamYUAAAAAEHlscv6pF7DTjPk4kapS8pXwYGJ",
  devMode: true,
  // 7 is month index
  voteEndDate: new Date(2018, 7, 14, 0, 0, 0, 0),
  minDob: new Date(1918, 0, 0),
  maxDob: new Date(2001, 0, 0)
}
