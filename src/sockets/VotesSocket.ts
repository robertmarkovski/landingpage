
import * as SocketIO from "socket.io";

export class VotesSocket {
  private socket: SocketIO.Socket;
  private io: SocketIO.Server;

  public init(io: SocketIO.Server) {
    this.io = io;
    io.on('connection', (socket) => {
      console.log("Votes connected");

      this.socket = socket;
    });
  }

  public emitVoted() {
    console.log("Vote count changed");
    this.io.emit("voted");
  }
}

export const votesSocket = new VotesSocket();
