import * as express from 'express';
import IServer from '../interfaces/ServerInterface';
import * as socket from 'socket.io';
import {votesSocket} from "./VotesSocket";

export default class Sockets {
  static init(server: IServer) :void {
    const ioServer = require('http').Server(server.app);
    ioServer.listen(8080);

    const io = socket(ioServer);

    votesSocket.init(io);
    io.on('connection', function (socket) {
      console.log("Connection");
    });
  }
}
