import { model, Schema } from 'mongoose';

const VoteModel: Schema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
      type: String,
      required: true
    },
    residence: {
      type: Number,
      required: true
    },
    dob: {
      type: Date,
      required: true
    },
    ip: {
      type: String,
      required: true
    },
    vote: {
        type: Number,
        required: true
    }
},
    {
        collection: 'votemodel',
        versionKey: false
    },
);

export default model('VoteModel', VoteModel);
