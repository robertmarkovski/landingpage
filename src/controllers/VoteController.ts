import VoteModel from '../models/VoteModel';
import * as express from 'express';
import {votesSocket} from "../sockets/VotesSocket";
import axios from 'axios';
import config from "../config/config";

const validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

class VoteController {
  public getVotesFor(vote?: number) {
    const pipeline = [];
    if (vote !== null) {
      pipeline.push({ $match: { vote: vote } })
    }
    pipeline.push({
      $group: {_id: "$vote", votes: {$sum: 1}}
    });
    return VoteModel
      .aggregate(pipeline);
  }

  public getHandlerFor(vote?: number) {
    return (req, res, next ) => {
      this.getVotesFor(vote).then((data) => {
        res.status(200).json({data});
      })
        .catch((error: Error) => {
          res.status(500).json({
            error: error.message,
            errorStack: error.stack
          });
          next(error);
        });
    };
  }

  

  public async submitVote(req: express.Request, res: express.Response, next: express.NextFunction) {
    const now = new Date();
    if (now > config.voteEndDate) {
      return res.status(400).json({message: "Vor- und Nachname eingeben"});
    }

    if (!req.body.firstName) {
      return res.status(400).json({message: "Vorname eingeben"});
    }

    if (!req.body.lastName) {
      return res.status(400).json({message: "Nachname eingeben"});
    }

    if (!validateEmail(req.body.email)) {
      return res.status(400).json({message: "Email-Adresse eingeben"});
    }

    if (!req.body.dob) {
      return res.status(400).json({message: "Geburtsdatum eingeben"});
    }

    const dob = new Date(req.body.dob);
    if (!dob) {
      return res.status(400).json({message: "Bitte Geburtsdatum prüfen"});
    }

    if (dob < config.minDob || dob > config.maxDob) {
      return res.status(400).json({message: "Bitte Geburtsdatum prüfen"});
    }

    if (!req.body.residence) {
      return res.status(400).json({message: "Wohnort auswählen"});
    }

    if (!req.body.acceptTos) {
      return res.status(400).json({message: "Teilnahmebedingungen akzeptieren"});
    }

    const ip = req.connection.remoteAddress;

    // Check for duplicates and validate captcha in prod.
    if (!config.devMode) {
      if (!req.body.captcha) {
        res.status(400).json({message: "Captcha bestätigen"});
        return;
      }

      try {
        const recaptchaResponse = await axios.get("https://www.google.com/recaptcha/api/siteverify", {
          params: {
            response: req.body.captcha,
            secret: config.recaptchaSecret,
            remoteip: ip
          }
        });
        if (!recaptchaResponse.data.success) {
          console.log(recaptchaResponse.data);
          throw new Error("Invalid captcha");
        }
      } catch (err) {
        console.log(err);
        res.status(400).json({message: "Captcha bestätigen"});
        return;
      }

      const exists = await VoteModel.findOne({email: req.body.email});

      if (exists) {
        res.status(400).json({message: "Email already registered"});
        return;
      }
    }


    VoteModel
      .create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        dob: new Date(req.body.dob),
        vote: req.body.vote,
        residence: req.body.residence,
        ip
      })
      .then((data) => {
        votesSocket.emitVoted();
        res.status(200).json({data});
      })
      .catch((error: Error) => {
        res.status(500).json({
          error: error.message,
          errorStack: error.stack
        });
        next(error);
      });
  }
}

export default new VoteController();
